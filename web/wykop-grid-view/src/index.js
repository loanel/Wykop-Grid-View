import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class Wykop extends React.Component {
	render() {
		return (
			<div>
				Hello Wykop
			</div>
		);
	}
}

class Site extends React.Component {
  render() {
    return (
		<div>
			<div>
				<Wykop />
			</div>
		</div>
    );
  }
}

// ========================================

ReactDOM.render(
  <Site />,
  document.getElementById('root')
);
	