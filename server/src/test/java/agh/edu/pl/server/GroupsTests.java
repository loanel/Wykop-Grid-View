package agh.edu.pl.server;

import agh.edu.pl.server.model.Group;
import agh.edu.pl.server.repository.GroupsRepository;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles(value = "test")
public class GroupsTests {
    @Autowired
    private TestRestTemplate testRestTemplate;
    private static String GROUP_ENDPOINT = "/groups";

    @Test
    public void serverRunningTest() {
        ///given
        ///when
        ResponseEntity<String> response = this.testRestTemplate.getForEntity("/", String.class);

        ///then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void groupPutTest() throws JSONException {

        ///given
        Group group = new Group("TestGroup");
        this.testRestTemplate.postForEntity(GROUP_ENDPOINT, group, Group.class);

        ///when
        String jsonResponse = testRestTemplate.getForObject(GROUP_ENDPOINT, String.class);
        JSONObject jsonObj = new JSONObject(jsonResponse).getJSONObject("_embedded");
        JSONArray jsonArray = jsonObj.getJSONArray("groups");

        ///then
        assertEquals("TestGroup", jsonArray.getJSONObject(0).getString("groupName"));
    }

    @Test
    public void groupAddTagTest() throws JSONException {
        ///given
        Group group = new Group("TestGroup");
        String stringToAdd = "testTagString";
        this.testRestTemplate.postForEntity(GROUP_ENDPOINT, group, Group.class);
        this.testRestTemplate.postForEntity(GROUP_ENDPOINT + "/1/addTag", stringToAdd, String.class);

        ///when
        String jsonResponse = testRestTemplate.getForObject(GROUP_ENDPOINT + "/1", String.class);

        ///then
        assertThat(jsonResponse.contains(stringToAdd));
    }

    @Test
    public void groupPutTagTest() throws JSONException{

        ///given
        Group group = new Group("TestGroup");
        List<String> stringList = new ArrayList<>();
        stringList.add("testString1");
        stringList.add("testString2");
        stringList.add("testString3");
        this.testRestTemplate.postForEntity(GROUP_ENDPOINT, group, Group.class);
        this.testRestTemplate.put(GROUP_ENDPOINT + "/1", stringList);

        ///when
        String jsonResponse = testRestTemplate.getForObject(GROUP_ENDPOINT + "/1", String.class);

        ///then
        for(String str: stringList){
            assertThat(jsonResponse.contains(str));
        }

    }

    @Test
    public void groupDeleteTagTest() throws JSONException{
        ///given
        Group group = new Group("TestGroup");
        List<String> stringList = new ArrayList<>();
        stringList.add("testString1");
        stringList.add("testString2");
        stringList.add("testString3");
        this.testRestTemplate.postForEntity(GROUP_ENDPOINT, group, Group.class);
        this.testRestTemplate.put(GROUP_ENDPOINT + "/1", stringList);

        ///when
        stringList.remove("testString1");
        this.testRestTemplate.put(GROUP_ENDPOINT + "/1", stringList);
        String jsonResponse = testRestTemplate.getForObject(GROUP_ENDPOINT + "/1", String.class);


        //then
        assertThat(!jsonResponse.contains("testString1"));

    }
}
