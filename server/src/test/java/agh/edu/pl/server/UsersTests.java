package agh.edu.pl.server;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles(value = "test")
public class UsersTests {

    @Autowired
    private TestRestTemplate testRestTemplate;
    private static String USER_ENDPOINT = "/users";


    @Test
    public void serverRunningTest() {
        ///given
        ///when
        ResponseEntity<String> response = this.testRestTemplate.getForEntity("/", String.class);

        ///then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

}
