package agh.edu.pl.server.repository;

import agh.edu.pl.server.model.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.webmvc.RepositoryRestController;

@RepositoryRestController
public interface GroupsRepository extends JpaRepository<Group, Integer> {
}
