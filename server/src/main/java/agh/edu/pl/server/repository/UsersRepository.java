package agh.edu.pl.server.repository;

import agh.edu.pl.server.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<User, Integer> {
    User findByPrincipalId(String principalId);
}
