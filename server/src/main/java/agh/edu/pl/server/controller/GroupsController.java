package agh.edu.pl.server.controller;

import agh.edu.pl.server.dto.PostDto;
import agh.edu.pl.server.dto.PostDtoBuilder;
import agh.edu.pl.server.model.Group;
import agh.edu.pl.server.repository.GroupsRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RepositoryRestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class GroupsController {
    @Autowired
    GroupsRepository groupsRepository;

    private final String wykop_app_key;


    GroupsController(){
        this.wykop_app_key = System.getenv("WYKOP_APPKEY");
    }

    @RequestMapping(value = "/groups/{id}/addTag", method = RequestMethod.POST)
    public ResponseEntity<String> addTag(@PathVariable Integer id, @RequestBody String tag) {
        Group group = groupsRepository.getOne(id);
        group.addTag(tag);
        groupsRepository.save(group);
        return new ResponseEntity<String>("Added tag to group", HttpStatus.OK);
    }

    @RequestMapping(value = "/groups/{id}/deleteTag", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteTag(@PathVariable Integer id, @RequestBody String tag){
        Group group = groupsRepository.getOne(id);
        group.removeTag(tag);
        groupsRepository.save(group);
        return new ResponseEntity<String>("Removed tag from group", HttpStatus.OK);
    }


    @RequestMapping(value = "/groups/{id}/posts", method = RequestMethod.GET)
    public ResponseEntity<List<PostDto>>  getPosts(@PathVariable Integer id) throws IOException {
        Group group = groupsRepository.getOne(id);
        System.out.println(" " + group.getGroupName());
        group.getTagList().stream()
                .forEach(t -> System.out.println(t));
        String apiRequest = "http://a.wykop.pl/tag/index/";
        RestTemplate restTemplate = new RestTemplate();
        List<PostDto> responseList = new ArrayList<>();

        for (String tag : group.getTagList()) {
            ResponseEntity<String> response = restTemplate.getForEntity(apiRequest + tag + "/appkey," + wykop_app_key + ",page,1", String.class);
            System.out.println(response.getStatusCode());
            JSONObject jsonObj = new JSONObject(response.getBody());
            System.out.println(response.getBody());
            JSONArray jsonArray = jsonObj.getJSONArray("items");
            for(int i = 0; i < 10; i++){
                System.out.println(i);
                if(!jsonArray.getJSONObject(i).has("body")) continue;
                responseList.add(createPostDto(jsonArray.getJSONObject(i), tag));
            }
        }

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("posts", "true");
        return new ResponseEntity<List<PostDto>>(responseList, responseHeaders, HttpStatus.OK);
    }

    private PostDto createPostDto(JSONObject jsonObject, String tag){
        return new PostDtoBuilder()
                .setAuthor(jsonObject.getString("author"))
                .setBody(jsonObject.getString("body"))
                .setAvatarUrl(jsonObject.getString("author_avatar"))
                .setUrl(jsonObject.getString("url"))
                .setTag(tag)
                .createPostDto();
    }
}
