package agh.edu.pl.server;

import agh.edu.pl.server.model.User;
import agh.edu.pl.server.model.UserLoginType;
import agh.edu.pl.server.repository.UsersRepository;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.*;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Configuration
@EnableOAuth2Sso
@Profile(value = {"development", "production"})
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception{

        http
                .csrf()
                .disable()
                .antMatcher("/**")
                    .authorizeRequests()
                .antMatchers("/", "index.html", "/h2-console")
                    .permitAll()
                .anyRequest()
                    .authenticated();
        http
                .headers()
                .frameOptions()
                .disable();
    }

    @Bean
    public PrincipalExtractor principalExtractor(UsersRepository usersRepository){
        return map -> {
            String principalId = (String) map.get("id");
            User user = usersRepository.findByPrincipalId(principalId);
            if(user == null){
                user = new User();
                user.setPrincipalId(principalId);
                user.setCreated(LocalDateTime.now());
                user.setEmail((String) map.get("email"));
                user.setLoginType(UserLoginType.GOOGLE);
                user.setLastLogin(LocalDateTime.now());
            } else {
                user.setLastLogin(LocalDateTime.now());
            }

            usersRepository.save(user);
            return user;
        };
    }
}
