package agh.edu.pl.server.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name="USERS")
public class User {
    @Id
    @GeneratedValue
    private Integer id;
    private String email;
    private String principalId;
    private LocalDateTime created;
    private LocalDateTime lastLogin;
    @Enumerated(EnumType.STRING)
    private UserLoginType loginType;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<Group> groups;


    public Integer getId(){
        return id;
    }

    public String getEmail(){
        return email;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public String getPrincipalId(){
        return principalId;
    }

    public void setPrincipalId(String principalId){
        this.principalId = principalId;
    }
    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(LocalDateTime lastLogin) {
        this.lastLogin = lastLogin;
    }

    public UserLoginType getLoginType() {
        return loginType;
    }

    public void setLoginType(UserLoginType loginType) {
        this.loginType = loginType;
    }

    public void addGroup(Group group){
        groups.add(group);

        group.setUser(this);
    }

    public void removeGroup(Group group){
        groups.remove(group);
        group.setUser(null);
    }

    public Set<Group> getGroups(){
        return groups;
    }
}
