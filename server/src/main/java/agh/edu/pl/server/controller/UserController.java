package agh.edu.pl.server.controller;

import agh.edu.pl.server.model.Group;
import agh.edu.pl.server.model.User;
import agh.edu.pl.server.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
public class UserController {
    @Autowired
    UsersRepository usersRepository;

    @RequestMapping("/user")
    public Principal user(Principal principal){
        return principal;
    }
}
