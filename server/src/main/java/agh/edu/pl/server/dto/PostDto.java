package agh.edu.pl.server.dto;

public class PostDto {
    private final String tag;
    private final String author;

    public String getTag() {
        return tag;
    }

    public String getAuthor() {
        return author;
    }

    public String getBody() {
        return body;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getUrl() {
        return url;
    }

    private final String body;
    private final String avatarUrl;
    private final String url;


    public PostDto(String tag, String author, String body, String avatarUrl, String url) {
        this.tag = tag;
        this.author = author;
        this.body = body;
        this.avatarUrl = avatarUrl;
        this.url = url;
    }


}
