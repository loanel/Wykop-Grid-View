package agh.edu.pl.server.dto;

public class PostDtoBuilder {
    private String tag;
    private String author;
    private String body;
    private String avatarUrl;
    private String url;

    public PostDtoBuilder setTag(String tag) {
        this.tag = tag;
        return this;
    }

    public PostDtoBuilder setAuthor(String author) {
        this.author = author;
        return this;
    }

    public PostDtoBuilder setBody(String body) {
        this.body = body;
        return this;
    }

    public PostDtoBuilder setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
        return this;
    }

    public PostDtoBuilder setUrl(String url) {
        this.url = url;
        return this;
    }

    public PostDto createPostDto() {
        return new PostDto(tag, author, body, avatarUrl, url);
    }
}