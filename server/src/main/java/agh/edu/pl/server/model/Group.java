package agh.edu.pl.server.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "GROUPS")
public class Group {
    @Id
    @GeneratedValue
    private Integer id;

    private String groupName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user_id")
    private User user;

    @ElementCollection
    Set<String> tagList;

    public Group(){}

    public Group(String groupName){
        this.groupName = groupName;
    }


    public Integer getId(){
        return id;
    }

    public String getGroupName(){
        return groupName;
    }

    public void setGroupName(String groupName){
        this.groupName = groupName;
    }

    public void addTag(String tag){
        tagList.add(tag);
    }

    public void removeTag(String tag){
        tagList.remove(tag);
    }

    public Set<String> getTagList(){
        return tagList;
    }

    public User getUser(){
        return user;
    }

    public void setUser(User user){
        this.user = user;
    }

}
